var http = require("http");
var fs = require("fs");
var qs = require("querystring");

var contentTypes = [
    {extension: "/", contentType: "text/html"},
    {extension: "css", contentType: "text/css"},
    {extension: "js", contentType: "text/plain"},
    {extension: "jpg", contentType: "image/jpeg"},
    {extension: "png", contentType: "image/png"},
]

//var messages = []
var requests = []

//sending data to waiting users
function sendData(message){
    console.log(message)
    console.log(requests.length)
    for(var i=0; i<requests.length; i++){
        if(requests[i]){
            requests[i].response.writeHead(200, { 'Content-Type': 'text/html' });
            requests[i].response.end(JSON.stringify(message))
        }
    }
    requests = []
}

function _Message(res, count, nick){
    this.nick = nick
    var that = this
    this.response = res
    var counter = count
    setTimeout(function() {
        requests[counter] = null
        that.response.writeHead(200, { 'Content-Type': 'text/html' });
        that.response.end(JSON.stringify(""))
    }, 2500);
}

function servResponse(req,res){
    var allData = "";
    req.on("data", function (data) {
        allData += data;
    })
    req.on("end", function (data) {
        var finishObj = qs.parse(allData)
        switch (finishObj.action) {
            case "sendData":
                res.writeHead(200, { 'Content-Type': 'text/html' });
                res.end(JSON.stringify("succes"))
                sendData(finishObj)
                break;
            case "getData":
                for(var i=requests.length-1; i>=0; i--){
                    if( requests[i] == null || (requests[i] != null && requests[i].nick == finishObj.nick) ){
                        requests.splice(i, 1);
                    }
                }

                requests.push(new _Message(res, requests.length, finishObj.nick))    
            //requests.push({request: req, response: res, nick: finishObj.nick, time: finishObj.time }) //wrzucenie tego do tablicy requestow
        }
    })
}

var server = http.createServer(function(request,response){
    switch (request.method) {
        case "GET":
        var file
        var contentType = null
        var extensionIndex = request.url.indexOf(".") 
        if(extensionIndex == -1){
            file = "static/index.html"
            contentType = "text/html"
        }else{
            for(var i=0; i<contentTypes.length; i++){
                if( request.url.slice(extensionIndex+1) == contentTypes[i].extension)
                    contentType = contentTypes[i].contentType
            }
            file = "static" + request.url
        }
        if(contentType){
            fs.readFile(file, function (error, data) {
                response.writeHead(200, { 'Content-Type': contentType });
                response.write(data);
                response.end();
            })
        }
        break;
        case "POST":
            servResponse(request,response)
            break;
    
    } 
})

server.listen(3000, function(){
   console.log("serwer startuje na porcie 3000")
});
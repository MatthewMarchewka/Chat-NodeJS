var user = null
visibleMessages = []

function logowanie(){
    $("#logowanie").toggle()
    user = new _User( prepareText( $("#nick").val() ) )
    net.getData()
}
/*
    input: minimum and maximum value of tab
    output: rounded value of random number
*/
function randomize(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min
}

function commandCheck(text){
    if(text.charAt(0) == "/"){
        if(text == "/nick"){
            var bubbleContainer = $('<div>',{class: "bubbleContainer"}).html("<br><hr>Twój nick to: <b>"+user.nick+"</b><hr><br>")
            $("#dataDiv").append(bubbleContainer)
            var objDiv = document.getElementById("dataDiv");
            objDiv.scrollTop = objDiv.scrollHeight;
        }else if(text == "/color"){
            var bubbleContainer = $('<div>',{class: "bubbleContainer"}).html("<br><hr>Twój kolor to: <b>"+user.color+"</b><hr><br>")
            $("#dataDiv").append(bubbleContainer)
            var objDiv = document.getElementById("dataDiv");
            objDiv.scrollTop = objDiv.scrollHeight;
        }else if(text == "/exit"){
            console.log("wylogowanie z serwera")
            $("#logowanie").toggle()
            user = null
            $("#dataDiv").empty()
        }else if(text == "/clear"){
            $("#dataDiv").empty()
        }else
            return false
    }else
        return false
    
    return true
}

function prepareText(text){
    return text.replace(/</g,"&lt;").replace(/>/g,"&gt;").replace(/"/g, "&quot;").replace(/'/g, "&#39;")
}

function emojiCreate(text){
    for(var i=0; i<emoji.length; i++){
        text = text.replace(emoji[i].raw , emoji[i].emoticon)
    }
    return text
}

for(var i=0; i<emoji.length; i++){
    var emojiEl = $('<span>',{class: "emoji"}).html(emoji[i].emoticon)
    emojiEl.on("click", function(){
        //this.innerHTML
        $("#messageInput").val( $("#messageInput").val() + this.innerHTML)
    })
    $("#emojiGrid").append(emojiEl)
}

$("#emojiGrid").toggle()

var net;
$(document).ready(function () {
      net = new Net();
})
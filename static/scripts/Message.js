function _Message(content,nick,color, hour, minute, source){

    this.content = content
    this.nick = nick
    this.color = color
    this.source = source
    this.hour = hour
    this.minute = minute

    function minuteFormat(minute){
        if(minute < 10){
            minute.toString()
            minute = "0"+minute
        }
        return minute
    }

    this.create = function(){
        //container for message
        var bubbleContainer = $('<div>',{class: "bubbleContainer"})
        $("#dataDiv").append(bubbleContainer)
        //message
        this.element = $('<div>',{class: "message"}).html(this.content).css({backgroundColor: this.color})
        this.element.addClass(source)
        //info element
        this.infoElement = $('<div>',{class: "infoElement"}).html(this.hour+":"+minuteFormat(this.minute))
        this.infoElement.toggle()
        this.element.append(this.infoElement)
        this.element.mouseenter( this.infoToggle.bind(null, this) )
        this.element.mouseleave( this.infoToggle.bind(null, this) )
        //user nick
        this.pictureElement = $('<div>',{class: "pictureElement"})
        this.nickElement = $('<div>',{class: "infoElement bottomInfo"}).html(this.nick).css("color", this.color)
        this.pictureElement.mouseenter( this.nickToggle.bind(null, this) )
        this.pictureElement.mouseleave( this.nickToggle.bind(null, this) )
        this.nickElement.toggle()
        this.element.append(this.nickElement)
        this.element.append(this.pictureElement)

        if(source == "incoming"){
            this.infoElement.addClass("rightInfo")
            this.pictureElement.addClass("rightPictureElement")
            this.nickElement.css("right", "-35px")
        }else{
            this.infoElement.addClass("leftInfo")
            this.pictureElement.addClass("leftPictureElement")
            this.nickElement.css("left", "-35px")
        }
        bubbleContainer.append(this.element)

        var objDiv = document.getElementById("dataDiv");
        objDiv.scrollTop = objDiv.scrollHeight;
    }

    this.infoToggle = function(object,click){
        object.infoElement.toggle()
    }
    this.nickToggle = function(object,click){
        object.nickElement.toggle()
    }

    this.create();
}
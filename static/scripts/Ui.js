$("#submitLogin").on("click", function(){
    if($("#nick").val() != ""){
        logowanie()
    }
})

document.addEventListener("keydown", function(click){
    if(click.key == "Enter"){
        if($("#logowanie").is(":visible") && $("#nick").val() != ""){
            logowanie()
        }else if(!$("#logowanie").is(":visible") && $("#messageInput").val() != "" ){
            if(!commandCheck($("#messageInput").val())){
                var now = new Date(Date.now())
                var text = emojiCreate( $("#messageInput").val() )
                text = prepareText(text)
                visibleMessages.push(new _Message( text, user.nick, user.color,now.getHours(),now.getMinutes(), "self"))
                net.sendData({action: "sendData", nick: user.nick.toString(), content: text, hour: now.getHours(), minutes: now.getMinutes(), color: user.color })
            }
            $("#messageInput").val("")
        }
    }else if(click.key == " " && document.activeElement.id == "messageInput"){
        $("#messageInput").val( emojiCreate($("#messageInput").val()) )
    }
})

$("#emojiOpen").on("click", function(){
    $("#emojiGrid").toggle()
})

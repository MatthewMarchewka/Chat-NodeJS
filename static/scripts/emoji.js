emoji = [
    {raw: ":)", emoticon: "🙂"},
    {raw: ":*", emoticon: "😘"},
    {raw: ":(", emoticon: "😔"},
    {raw: ":/", emoticon: "😕"},
    {raw: "<3", emoticon: "💗"},
    {raw: "</3", emoticon: "💔"},
    {raw: ">.<", emoticon: "😆"},
    {raw: ":P", emoticon: "😋"},
    {raw: ":o", emoticon: "😳"}
]